//ease functions

var ease = {

    //
    // Basic ease functions
    //
    
        easeConstant: function (time, begin, change, duration) {
            "use strict";
            return begin;
        },
        
        easeLinear: function (time, begin, change, duration) {
            "use strict";
            var val = time/duration;
            return begin + change * val;
        },
        
        easeInPower: function (time, begin, change, duration, power) {
            "use strict";
            var val;
            if (power === undefined) { power = 3; }
            val = Math.pow(time/duration, power);
            return begin + change * val;
        },
        
        easeOutPower: function (time, begin, change, duration, power) {
            "use strict";
            var val = tbEase.easeInPower(duration-time, 1, -1, duration, power);
            return begin + change * val;
        },
        
        easeInOutPower: function (time, begin, change, duration, power) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInPower(time, 0, 0.5, duration/2, power);
            } else {
                val = tbEase.easeInPower(duration-time, 1, -0.5, duration/2, power);
            }
            return begin + change * val;
        },
        
        easeOutInPower: function (time, begin, change, duration, power) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInPower(duration/2-time, 0.5, -0.5, duration/2, power);
            } else {
                val = tbEase.easeInPower(time-duration/2, 0.5, 0.5, duration/2, power);
            }
            return begin + change * val;
        },
        
        easeInQuadratic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeInPower(time, begin, change, duration, 2);
        },
        
        easeOutQuadratic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeOutPower(time, begin, change, duration, 2);
        },
        
        easeInOutQuadratic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeInOutPower(time, begin, change, duration, 2);
        },
        
        easeOutInQuadratic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeOutInPower(time, begin, change, duration, 2);
        },
        
        easeInCubic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeInPower(time, begin, change, duration, 3);
        },
        
        easeOutCubic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeOutPower(time, begin, change, duration, 3);
        },
        
        easeInOutCubic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeInOutPower(time, begin, change, duration, 3);
        },
        
        easeOutInCubic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeOutInPower(time, begin, change, duration, 3);
        },
        
        easeInQuartic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeInPower(time, begin, change, duration, 4);
        },
        
        easeOutQuartic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeOutPower(time, begin, change, duration, 4);
        },
        
        easeInOutQuartic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeInOutPower(time, begin, change, duration, 4);
        },
        
        easeOutInQuartic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeOutInPower(time, begin, change, duration, 4);
        },
        
        easeInQuintic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeInPower(time, begin, change, duration, 5);
        },
        
        easeOutQuintic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeOutPower(time, begin, change, duration, 5);
        },
        
        easeInOutQuintic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeInOutPower(time, begin, change, duration, 5);
        },
        
        easeOutInQuintic: function (time, begin, change, duration) {
            "use strict";
            return tbEase.easeOutInPower(time, begin, change, duration, 5);
        },
        
        easeInExponential: function (time, begin, change, duration, power) {
            "use strict";
            var val, base;
            if (power === undefined) { power = 8; }
            base = Math.pow(2, -power);			// The exponential function will not cover the whole range from 0-1 so we must correct it
            val = (Math.pow(2, power * time/duration - power) - base) / (1-base);
            return begin + change * val;
        },
        
        easeOutExponential: function (time, begin, change, duration, power) {
            "use strict";
            var val = tbEase.easeInExponential(duration-time, 1, -1, duration, power);
            return begin + change * val;
        },
        
        easeInOutExponential: function (time, begin, change, duration, power) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInExponential(time, 0, 0.5, duration/2, power);
            } else {
                val = tbEase.easeInExponential(duration-time, 1, -0.5, duration/2, power);
            }
            return begin + change * val;
        },
        
        easeOutInExponential: function (time, begin, change, duration, power) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInExponential(duration/2-time, 0.5, -0.5, duration/2, power);
            } else {
                val = tbEase.easeInExponential(time-duration/2, 0.5, 0.5, duration/2, power);
            }
            return begin + change * val;
        },
        
        easeInSine: function (time, begin, change, duration) {
            "use strict";
            var val = 1 - Math.cos(time/duration * Math.PI/2);
            return begin + change * val;
        },
        
        easeOutSine: function (time, begin, change, duration) {
            "use strict";
            var val = tbEase.easeInSine(duration-time, 1, -1, duration);
            return begin + change * val;
        },
        
        easeInOutSine: function (time, begin, change, duration) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInSine(time, 0, 0.5, duration/2);
            } else {
                val = tbEase.easeInSine(duration-time, 1, -0.5, duration/2);
            }
            return begin + change * val;
        },
        
        easeOutInSine: function (time, begin, change, duration) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInSine(duration/2-time, 0.5, -0.5, duration/2);
            } else {
                val = tbEase.easeInSine(time-duration/2, 0.5, 0.5, duration/2);
            }
            return begin + change * val;
        },
        
        easeInCircular: function (time, begin, change, duration) {
            "use strict";
            var val = 1 - Math.sqrt(1 - Math.pow(time/duration, 2));
            return begin + change * val;
        },
        
        easeOutCircular: function (time, begin, change, duration) {
            "use strict";
            var val = tbEase.easeInCircular(duration-time, 1, -1, duration);
            return begin + change * val;
        },
        
        easeInOutCircular: function (time, begin, change, duration) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInCircular(time, 0, 0.5, duration/2);
            } else {
                val = tbEase.easeInCircular(duration-time, 1, -0.5, duration/2);
            }
            return begin + change * val;
        },
        
        easeOutInCircular: function (time, begin, change, duration) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInCircular(duration/2-time, 0.5, -0.5, duration/2);
            } else {
                val = tbEase.easeInCircular(time-duration/2, 0.5, 0.5, duration/2);
            }
            return begin + change * val;
        },
        
        easeInElastic: function (time, begin, change, duration, oscillations, stiffness) {
            "use strict";
            var val;
            if (oscillations === undefined) { oscillations = 3; }
            if (stiffness === undefined) { stiffness = 8; }
            val = Math.sin((1-time/duration)*2*Math.PI*oscillations + Math.PI/2);			// Create the correct sign
            val *= tbEase.easeInExponential(time, 0, 1, duration, stiffness);				// Multiply with an exponential ease in
            return begin + change * val;
        },
    
        easeOutElastic: function (time, begin, change, duration, oscillations, stiffness) {
            "use strict";
            var val = tbEase.easeInElastic(duration-time, 1, -1, duration, oscillations, stiffness);
            return begin + change * val;
        },
        
        easeInOutElastic: function (time, begin, change, duration, oscillations, stiffness) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInElastic(time, 0, 0.5, duration/2, oscillations, stiffness);
            } else {
                val = tbEase.easeInElastic(duration-time, 1, -0.5, duration/2, oscillations, stiffness);
            }
            return begin + change * val;
        },
        
        easeOutInElastic: function (time, begin, change, duration, oscillations, stiffness) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInElastic(duration/2-time, 0.5, -0.5, duration/2, oscillations, stiffness);
            } else {
                val = tbEase.easeInElastic(time-duration/2, 0.5, 0.5, duration/2, oscillations, stiffness);
            }
            return begin + change * val;
        },
        
        easeInOvershoot: function (time, begin, change, duration, overshoot) {
            "use strict";
            var val;
            if (overshoot === undefined) { overshoot = 1.70158; }
            val = (1+overshoot) * Math.pow(time/duration, 3) - overshoot * Math.pow(time/duration, 2);
            return begin + change * val;
    
        },
        
        easeOutOvershoot: function (time, begin, change, duration, overshoot) {
            "use strict";
            var val = tbEase.easeInOvershoot(duration-time, 1, -1, duration, overshoot);
            return begin + change * val;
        },
        
        easeInOutOvershoot: function (time, begin, change, duration, overshoot) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInOvershoot(time, 0, 0.5, duration/2, overshoot);
            } else {
                val = tbEase.easeInOvershoot(duration-time, 1, -0.5, duration/2, overshoot);
            }
            return begin + change * val;
        },
        
        easeOutInOvershoot: function (time, begin, change, duration, overshoot) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInOvershoot(duration/2-time, 0.5, -0.5, duration/2, overshoot);
            } else {
                val = tbEase.easeInOvershoot(time-duration/2, 0.5, 0.5, duration/2, overshoot);
            }
            return begin + change * val;
        },
    
        easeInBounce: function (time, begin, change, duration, number, factor) {
            "use strict";
            var bounces, i, w, width, height, val;
            // Correct the arguments
            if (number === undefined) { number = 4; }
            if (factor === undefined) { factor = 2; } else { factor = Math.sqrt(factor); }
            // Create the list of bounces
            bounces = [1];
            width = 1;
            for (i = 1; i < number; i += 1) {
                bounces.push(bounces[i-1]*factor);
                width += bounces[i];
            }
            // Compute total size
            width -= bounces[number-1]/2;		// We don't use the last half bounce
            height = Math.pow(bounces[number-1],2);
            time = time/duration * width;
            // Find the correct bounce
            w = 0;
            for (i = 0; i < number; i += 1) {
                if (time > bounces[i]) { time -= bounces[i];} else { w = bounces[i]; break; }
            }
            // Now compute the value
            val = -4 * Math.pow(time-w/2,2) + w * w;
            return begin + change * val / height;
        },
    
        easeOutBounce: function (time, begin, change, duration, number, factor) {
            "use strict";
            var val = tbEase.easeInBounce(duration-time, 1, -1, duration, number, factor);
            return begin + change * val;
        },
        
        easeInOutBounce: function (time, begin, change, duration, number, factor) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInBounce(time, 0, 0.5, duration/2, number, factor);
            } else {
                val = tbEase.easeInBounce(duration-time, 1, -0.5, duration/2, number, factor);
            }
            return begin + change * val;
        },
        
        easeOutInBounce: function (time, begin, change, duration, number, factor) {
            "use strict";
            var val;
            if (time < duration/2) {
                val = tbEase.easeInBounce(duration/2-time, 0.5, -0.5, duration/2, number, factor);
            } else {
                val = tbEase.easeInBounce(time-duration/2, 0.5, 0.5, duration/2, number, factor);
            }
            return begin + change * val;
        }
    };