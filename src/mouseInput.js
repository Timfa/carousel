class MouseInput 
{
    constructor(canvas)
    {
        this.trueMousePosition = new vector(0, 0)
        this.mousePosition = new vector(0, 0)
        MouseInput.Instance = this;

        document.onmousedown = function(e)
        {
            switch(e.button)
            {
                case MouseInput.MouseButtons.Left: 	MouseInput.Instance.g_mouseDownThisFrame = true; break;
                case MouseInput.MouseButtons.Middle: MouseInput.Instance.g_middleMouseDownThisFrame = true; break;
                case MouseInput.MouseButtons.Right: MouseInput.Instance.g_altMouseDownThisFrame = true; break;
            }
        }

        document.onmouseup = function(e)
        {
            switch(e.button)
            {
                case MouseInput.MouseButtons.Left: 	
                    MouseInput.Instance.g_mouseDownThisFrame = false; 
                    MouseInput.Instance.g_mouseDown = false;
                break;
                case MouseInput.MouseButtons.Middle: 
                    MouseInput.Instance.g_middleMouseDownThisFrame = false; 
                    MouseInput.Instance.g_middleMouseDown = false;
                break;
                case MouseInput.MouseButtons.Right: 	
                    MouseInput.Instance.g_altMouseDownThisFrame = false; 
                    MouseInput.Instance.g_altMouseDown = false;
                break;
            }

            for(let i = 0; i < DynamicObject.all.length; i++)
            {
                DynamicObject.all[i].onMouseRelease(MouseInput.Instance, e.button);
            }
        }

        document.onmousemove = function(e)
        {
            MouseInput.Instance.mousePosition.x = e.x + canvas.position.x;
            MouseInput.Instance.mousePosition.y = e.y + canvas.position.y;
        }
    }

    update()
    {
        if (MouseInput.Instance.g_mouseDown && MouseInput.Instance.g_mouseDownThisFrame)
		{
			MouseInput.Instance.g_mouseDownThisFrame = false;
		}
		
		if(MouseInput.Instance.g_mouseDownThisFrame && !MouseInput.Instance.g_mouseDown)
		{
			MouseInput.Instance.g_mouseDown = true;
		}

		if (MouseInput.Instance.g_altMouseDown && MouseInput.Instance.g_altMouseDownThisFrame)
		{
			MouseInput.Instance.g_altMouseDownThisFrame = false;
		}
		
		if(MouseInput.Instance.g_altMouseDownThisFrame && !MouseInput.Instance.g_altMouseDown)
		{
			MouseInput.Instance.g_altMouseDown = true;
		}

		if (MouseInput.Instance.g_middleMouseDown && MouseInput.Instance.g_middleMouseDownThisFrame)
		{
			MouseInput.Instance.g_middleMouseDownThisFrame = false;
		}
		
		if(MouseInput.Instance.g_middleMouseDownThisFrame && !MouseInput.Instance.g_middleMouseDown)
		{
			MouseInput.Instance.g_midleMouseDown = true;
		}

        if(this.mousePressed(MouseInput.MouseButtons.Left))
        {
            for(let i = 0; i < DynamicObject.all.length; i++)
            {
                DynamicObject.all[i].g_checkClick(this, true);
            }
        }

        if(this.mouseHeld(MouseInput.MouseButtons.Left))
        {
            for(let i = 0; i < DynamicObject.all.length; i++)
            {
                DynamicObject.all[i].g_checkClick(this, false);
            }
        }
    }

    mousePressed(btn)
	{
		switch(btn)
		{
			case MouseInput.MouseButtons.Left: 	return MouseInput.Instance.g_mouseDownThisFrame;
			case MouseInput.MouseButtons.Middle: return MouseInput.Instance.g_middleMouseDownThisFrame;
			case MouseInput.MouseButtons.Right: return MouseInput.Instance.g_altMouseDownThisFrame;
		}

		return this.g_mouseDownThisFrame;
	}

	mouseHeld(btn)
	{
		switch(btn)
		{
			case MouseInput.MouseButtons.Left: 	return MouseInput.Instance.g_mouseDown;
			case MouseInput.MouseButtons.Middle: return MouseInput.Instance.g_middleMouseDown;
			case MouseInput.MouseButtons.Right: return MouseInput.Instance.g_altMouseDown;
		}

		return this.g_mouseDown;
	}
}

MouseInput.MouseButtons = {
    Left: 0,
    Middle: 1,
    Right: 2
}