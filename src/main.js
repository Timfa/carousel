var canvasObj;
var mouseInputObj;

function init()
{
    loadImages();
    console.log("Starting");
    canvasObj = new Canvas(document.getElementById("canvas"), 0, 0, 800, 600)
    mouseInputObj = new MouseInput(canvasObj);

    canvasObj.redraw();
    console.log("Loop started")

    new Horse(200, 200, Image.loaded.block, Image.loaded.tinyblock)
    new Horse(300, 200, Image.loaded.block, Image.loaded.tinyblock)
}

function loadImages()
{
    Image.load("block", "img/block.png");
    Image.load("tinyblock", "img/tinyblock.png");
}

window.onload = init;