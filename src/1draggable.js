class Draggable extends DynamicObject
{
    dragging = false;
    offset = new vector();

    onClick(input)
    {
        this.dragging = true;
        this.offset = MouseInput.Instance.mousePosition.subtract(this.position)
    }

    onMouseRelease(input, btn)
    {
        this.dragging = false;
    }

    update(deltaTime)
    {
        super.update(deltaTime);

        if(this.dragging)
        {
            this.position.x = MouseInput.Instance.mousePosition.x - this.offset.x;
            this.position.y = MouseInput.Instance.mousePosition.y - this.offset.y;
        }
    }
}