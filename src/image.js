class Image
{
    constructor (name, url, subImgTotal, perRow)
    {
        this.subImgTotal = subImgTotal || 1;
        this.perRow = perRow || 1;

        this.subImages = this.subImgTotal;
        this.subImagesPerRow = this.perRow;

        
        this.body = document.body;
        this.body.insertAdjacentHTML('beforeend', '<img id="image" src="' + url + '" style="display:none;"></img>');

        this.g_image = document.getElementById('image');

        document.getElementById('image').id = "loadedImage";
    }

	static load(name, url, subImgTotal = 1, perRow = 1)
	{
		Image.loaded[name] = new Image(name, url, subImgTotal, perRow)
		console.log("loaded " + name, Image.loaded[name])
	}
	
	get width()
    {
        return this.g_image.width / this.subImagesPerRow;
	}

	get height()
    {
        return this.g_image.height / (Math.ceil(this.subImages / this.subImagesPerRow) + 0);
    }

	draw = function(sx, sy, sw, sh, dx, dy, dw, dh, rotation, alpha = 1)
	{	
		var shouldDraw = true;
		
		if(dx < -Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
			shouldDraw = false;
			
		if(dy < -Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
			shouldDraw = false;
		
		if(dx > Canvas.Instance.width + Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
			shouldDraw = false;
		
		if(dy > Canvas.Instance.height + Math.sqrt(Math.pow(dw, 2) + Math.pow(dh, 2)))
			shouldDraw = false;

		if(shouldDraw)
		{
			var context = Canvas.Instance.canvasContext;
			context.save(); 
			var oldAlpha = context.globalAlpha;
			context.globalAlpha = alpha;

			context.translate(dx, dy);
			context.rotate(rotation * (Math.PI/180));

			context.drawImage(this.g_image, sx, sy, sw, sh, -dw/2, -dh/2, dw, dh);

			context.restore(); 
			context.globalAlpha = oldAlpha;
		}
	}
}

Image.loaded = {};