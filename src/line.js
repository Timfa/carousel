class Line
{
    p1 = new vector();
    p2 = new vector();
    p3 = new vector();
    p4 = new vector();

    Lerp(t)
    {
        return this.p1.lerpTo(
        this.p2.lerpTo(
            this.p3.lerpTo(
                this.p4
                , t)
            , t)
        , t)
    }

    Draw(ctx)
    {
        ctx.lineWidth = 6;
        ctx.beginPath();
        ctx.moveTo(this.p1.x, this.p1.y);

        let resolution = 20;
        for(let i = 0; i < resolution; i++)
        {
            let nvector = this.Lerp(i / resolution)
            ctx.lineTo(nvector.x, nvector.y)
        }
        
        ctx.lineTo(this.p4.x, this.p4.y);


        //ctx.bezierCurveTo(this.p2.x, this.p2.y, this.p3.x, this.p3.y, this.p4.x, this.p4.y);
        ctx.stroke();
    }

    Clone()
    {
        let ln = new Line();
        ln.p1 = this.p1;
        ln.p2 = this.p2;
        ln.p3 = this.p3;
        ln.p4 = this.p4;
        return ln;
    }
}