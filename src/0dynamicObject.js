class DynamicObject
{
    subImage = 0; 
    g_subImgOld = 0;
    image;

    constructor(x, y, image)
    {
        DynamicObject.all = DynamicObject.all || [];
        DynamicObject.all.push(this);

        this.image = image;
        this.position = new vector(x, y)
        this.rotation = 0;

        this.scale = new vector(1, 1)
        this.alpha = 255;
    }

    left()
    {
        return this.g_dir(0);
    }
    
    right()
    {
        return this.g_dir(180);
    }
    
    forward()
    {
        return this.g_dir(90);
    }
    
    back()
    {
        return this.g_dir(270);
    }

    g_dir(offset)
    {
        var myAngleInRadians = (this.rotation + offset) * (Math.PI / 180);
        
        return new vector( -Math.cos(myAngleInRadians), -Math.sin(myAngleInRadians) );
    }

    update(deltaTime)
    {

    }

    draw(ctx)
    {
        if(this.image)
        {
            this.drawImg(this.position.x, this.position.y, 
                            this.image.width * this.scale.x, 
                            this.image.height * this.scale.y, 
                            this.rotation);
        }
    }
    
    g_checkClick (input, thisFrame)
    {
        var hit = false;
        var clickPos = input.mousePosition;
        
        //hw = halfWidth, hh = halfHeight
        var hw = this.image.width / 2;
        var hh = this.image.height / 2;
        
        if(clickPos.x > this.position.x - hw && clickPos.x < this.position.x + hw)
        {
            if(clickPos.y > this.position.y - hh && clickPos.y < this.position.y + hh)
            {
                hit = true;
            }
        }

        if (hit)
        {
            if (thisFrame)
            {
                this.onClick(input);
            }
            else
            {
                this.onMouseHeld(input);
            }
        }
    }
    
    onClick(input){}

    onMouseHeld(input){}

    onMouseRelease(input, btn){}

    drawImg(x, y, w, h, rotation)
    {
        if(isNaN(this.subImage))
        {
            this.subImage = this.g_subImgOld;
        }
        else
        {
            this.g_subImgOld = this.subImage;
        }

        var subImage = Math.round(this.subImage);
        subImage %= this.image.subImages;
        
        var c = subImage % this.image.subImagesPerRow;
        var r = Math.floor(subImage / this.image.subImagesPerRow);
        var sw = (this.image.g_image.width / this.image.subImagesPerRow);
        var sh = (this.image.g_image.height / Math.ceil(this.image.subImages / this.image.subImagesPerRow));
        var sx = sw * c;
        var sy = sh * r;

        this.image.draw(sx, sy, sw, sh, x, y, w, h, rotation, this.alpha);
    }
}