/**
 * Extending exising functionality
 */

 Array.prototype.remove = function(element)
 {
     var index = this.indexOf(element);
 
     if (index > -1) 
     {
         this.splice(index, 1);
     }
     else
     {
         console.error("Element not part of array!", this, element);
     }
 }
 
 Array.prototype.contains = function(element)
 {
     var index = this.indexOf(element);
 
     return index > -1;
 }
 
 Math.clamp = function(number, min, max)
 {
     return Math.min(max, Math.max(min, number));
 }

 Math.lerp = function(a, b, t)
 {
   return a + (b - a) * t
 }
 
 Array.prototype.random = function () 
 {
   return this[Math.floor((Math.random()*this.length))];
 }

 if (!Math.sign) {
    Math.sign = function(x) {
      x = +x; // convert to a number
      if (x === 0 || isNaN(x)) {
        return Number(x);
      }
      return x > 0 ? 1 : -1;
    };
  }
