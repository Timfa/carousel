class Horse extends DynamicObject
{
    courseMarker = null;
    path = [];
    line = null;
    linePoint = 0;
    tweenLine = null;

    get controlLength()
    {
        return Math.sqrt(Math.pow(this.path[Horse.currentPathPoint].pos.subtract(this.position).length(), 2) / 2)
    }

    constructor(x, y, image, courseImage)
    {
        super(x, y, image)
        this.courseMarker = new Draggable(x, y, courseImage)
        Horse.all.push(this);
        this.line = new Line();
    }

    static NextPoint()
    {
        if(Date.now() > Horse.enableTime)
        {
            Horse.currentPathPoint++;
            Horse.enableTime = Date.now() + 1200;
            Horse.ResetAllMarkers(true);
        }
    }

    static PreviousPoint()
    {
        if(Date.now() > Horse.enableTime)
        {
            if(Horse.currentPathPoint >= 1)
                Horse.currentPathPoint--;

            Horse.ResetAllMarkers(false);
        }
    }

    static ResetAllMarkers(next)
    {
        for(let i = 0; i < Horse.all.length; i++)
            Horse.all[i].resetMarker(next);
    }

    resetMarker(next)
    {
        if(Horse.currentPathPoint > 1 && next)
        {
            this.tweenLine = this.line.Clone();
            let horse = this;
            horse.linePoint = {p: 0};
            
            let tween = new TWEEN.Tween(horse.linePoint).to({p: 1}, 1000).easing(TWEEN.Easing.Quadratic.Out).onUpdate(function()
            {
                let newPosition = horse.tweenLine.Lerp(horse.linePoint.p);
                
                if(horse.linePoint.p > 0)
                {
                    let delta = newPosition.subtract(horse.position)
                    if(delta.length() > 0)
                        horse.rotation = delta.toAngle();
                }

                horse.position.x = newPosition.x;
                horse.position.y = newPosition.y;
            }).start();
        }
        else
        {
            this.position = next? this.courseMarker.position.getCopy() : this.path[Math.max(0, Horse.currentPathPoint - 1)].pos;
            this.rotation = next? this.courseMarker.rotation : this.path[Math.max(0, Horse.currentPathPoint - 1)].angle
        }

        this.path[Horse.currentPathPoint] = this.path[Horse.currentPathPoint] || {pos: next? this.courseMarker.position.getCopy() : this.path[Math.max(0, Horse.currentPathPoint - 1)].pos, angle: this.path[Math.max(0, Horse.currentPathPoint - 1)].angle};
        this.courseMarker.position = this.path[Horse.currentPathPoint].pos.getCopy()
        this.courseMarker.rotation = this.path[Horse.currentPathPoint].angle
    }

    update(deltaTime)
    {
        super.update(deltaTime);
        this.path[Horse.currentPathPoint] = this.path[Horse.currentPathPoint] || {pos: this.position.getCopy(), angle: this.rotation};
        let changed = this.path[Horse.currentPathPoint].pos.length() != this.courseMarker.position.length();
        this.path[Horse.currentPathPoint].pos = this.courseMarker.position.getCopy();

        if(Horse.currentPathPoint > 0)
        {
            let length = this.controlLength
            this.line.p1 = this.position.getCopy();
            this.line.p2 = this.position.add(this.forward().stretch(length))
            this.line.p3 = this.position.add(this.forward().stretch(length))
            this.line.p4 = this.courseMarker.position.getCopy();

            if(changed)
            {
                this.path[Horse.currentPathPoint].angle = this.line.p4.subtract(this.line.Lerp(0.95)).toAngle()
                this.courseMarker.rotation = this.path[Horse.currentPathPoint].angle;
            }
        }
        else
        {
            this.position = this.courseMarker.position.getCopy();
            this.path[Horse.currentPathPoint].angle = this.rotation;
            this.courseMarker.rotation = this.rotation;
        }
    }

    draw(ctx)
    {
        if(Horse.currentPathPoint > 0)
            this.line.Draw(ctx);
        super.draw(ctx);
    }
}

Horse.currentPathPoint = 0;
Horse.all = [];
Horse.enableTime = Date.now();