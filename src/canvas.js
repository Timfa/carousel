// Wrapper for some canvas drawing functions

class Canvas 
{
    position = new vector(0, 0)
    width = 0
	
	height = 0

	g_width = 0
	g_height = 0
    fullscreen = false

    backgroundColor = 
	{
		r: 255,
		g: 255,
		b: 255	
	}

    windowElement = null

    constructor(canvas, x, y, width, height)
	{
        Canvas.Instance = this;
        this.position = new vector(x, y)
		this.width = width;
		this.height = height;

		this.g_width = width;
		this.g_height = height;
		
		this.g_canvas = canvas;
		
		this.canvasContext = this.canvas.getContext('2d');

		console.log("Canvas initialized: [" + width + ", " + height + "]");
        this.stepPrevious = 0.001;
	}

    get canvas()
    {
        return this.g_canvas;
    }

    get context()
    {
        return this.canvasContext;
    }

    drawText(text, x, y, size, font, color)
    {
        this.context.font = font? size + 'px ' + font : size + 'px Arial';
        
        this.context.fillStyle = color;
        this.context.textAlign = "center";
        this.context.textBaseline="middle";

        this.context.fillText(text, x, y);
    }

    redraw()
	{
    	var stepTime = Date.now();

        this.stepPrevious = this.stepPrevious || 0.1

		var stepTimeCurr = (stepTime - this.stepPrevious) * 0.001;

        stepTimeCurr = Math.min(stepTimeCurr, 0.1);
        
		if(isNaN(stepTimeCurr))
            stepTimeCurr = 0;

        MouseInput.Instance.update();

        TWEEN.update();

		if(this.fullscreen)
		{
			this.width = window.innerWidth - 2;
			this.height = window.innerHeight - 2;
		}
		else
		{
			this.width = this.g_width;
			this.height = this.g_height;
		}

		this.canvas.width = this.width;
		this.canvas.height = this.height;

		this.canvasContext.fillStyle = "rgb(" + this.backgroundColor.r + "," + this.backgroundColor.g + "," + this.backgroundColor.b + ")"; // sets the color to fill in the rectangle with
		this.canvasContext.fillRect(0, 0, this.width, this.height);
		
        if(DynamicObject.all)
        {
            for(var i = 0; i < DynamicObject.all.length; i++)
            {
                DynamicObject.all[i].update(stepTimeCurr);
                DynamicObject.all[i].draw(this.canvasContext);
            }
        }

        this.stepPrevious = stepTime;
		requestAnimationFrame(function()
        {Canvas.Instance.redraw()});
	}
}